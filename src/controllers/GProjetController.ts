import { log } from "console";
import { Request, Response } from "express-serve-static-core";
import { normalize } from "path/posix";
import HomeController from "./HomeController";
export default class GProjetController {
    /**Affiche le formulaire
     * 
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void {
        res.render('pages/projet-create');
    }


    /**
     * Recupere le formulaire et insere l'article en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        db.prepare("INSERT INTO projets (nom, description, clients_idclients) VALUES (?, ?, ?)").run(req.body.nom, req.body.description, 1);

        HomeController.accueil(req, res);
    }
    /**
    * Affiche 1 projet
    * @param req 
    * @param res 
    */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const content = db.prepare('SELECT * FROM projets WHERE idprojets = ?').get(req.params.idprojets);

        res.render('pages/projet-read', {
            content: content
        });
    }


    /**
     * Affiche le formulaire pour modifier un article
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const projets = db.prepare("SELECT * FROM projets WHERE idprojets=?").get(req.params.idprojets);

        res.render('pages/projet-update', {
            projets: projets
        });
    }

    /**
     * Recupere le formulaire de l'article modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare("UPDATE projets SET nom = ?, description = ?, clients_idclients = ?  WHERE idprojets=?").run(req.body.nom, req.body.description, 1, req.params.idprojets);

        HomeController.accueil(req, res);
    }

    /**
     * Supprime un article
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare("DELETE FROM projets WHERE idprojets=?").run(req.params.idprojets);
        HomeController.accueil(req, res);
    }
}