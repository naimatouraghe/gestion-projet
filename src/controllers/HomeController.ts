import { Request, Response } from "express-serve-static-core";

export default class HomeController {
    static accueil(req: Request, res: Response): void {
        const db = req.app.locals.db;
        //const nom = req.body.nom;
        const projTitle = db.prepare('SELECT * FROM projets').all();

        //console.log(projTitle);

        res.render('pages/accueil', {

            projTitle: projTitle
        });
    }

}