INSERT INTO "devs" ( "nom", "prenom", "niveau")
VALUES ('Philippe', 'Lacroix', 'Tech Lead'), 
('Naima', 'Laneige', 'Junior'), 
('David', 'Lenon', 'Senior');

INSERT INTO "projets" ( "nom", "description", "clients_idclients")
VALUES ('Refonte du site web M2K', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed iaculis est. Vivamus eu fringilla leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 1),
('Site web du PSG', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed iaculis est. Vivamus eu fringilla leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 2),
('Application Mobile Tisséo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed iaculis est. Vivamus eu fringilla leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.', 3);

INSERT INTO "clients" ( "nom", "adresse")
VALUES ('Lebrun', '3 rue de la paix'), 
('Dubois', '42 rue du code'), 
('Favresse', '12 avenue de la Liberté');
