-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Naima
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 10:00
-- Created:       2021-12-20 09:23
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "devs"(
  "iddevs" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" LONG VARCHAR NOT NULL,
  "prenom" LONG VARCHAR NOT NULL,
  "niveau" LONG VARCHAR NOT NULL
);
CREATE TABLE "clients"(
  "idclients" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" LONG VARCHAR NOT NULL,
  "adresse" LONG VARCHAR NOT NULL
);
CREATE TABLE "projets"(
  "idprojets" INTEGER PRIMARY KEY AUTOINCREMENT,
  "nom" LONG VARCHAR,
  "description" LONGTEXT,
  "clients_idclients" INTEGER,
  CONSTRAINT "fk_projets_clients1"
    FOREIGN KEY("clients_idclients")
    REFERENCES "clients"("idclients")
);
CREATE INDEX "projets.fk_projets_clients1_idx" ON "projets" ("clients_idclients");
CREATE TABLE "projets_has_devs"(
  "projets_idprojets" INTEGER NOT NULL,
  "devs_iddevs" INTEGER NOT NULL,
  CONSTRAINT "fk_projets_has_devs_projets"
    FOREIGN KEY("projets_idprojets")
    REFERENCES "projets"("idprojets"),
  CONSTRAINT "fk_projets_has_devs_devs1"
    FOREIGN KEY("devs_iddevs")
    REFERENCES "devs"("iddevs")
);
CREATE INDEX "projets_has_devs.fk_projets_has_devs_devs1_idx" ON "projets_has_devs" ("devs_iddevs");
CREATE INDEX "projets_has_devs.fk_projets_has_devs_projets_idx" ON "projets_has_devs" ("projets_idprojets");
COMMIT;
