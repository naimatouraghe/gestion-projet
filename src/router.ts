import { Application } from "express";
import GProjetController from "./controllers/GProjetController";
import ArticleController from "./controllers/GProjetController";
import HomeController from "./controllers/HomeController";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.accueil(req, res);
    });

    app.get('/projet-read/:idprojets', (req, res) => {
        GProjetController.read(req, res)
    });

    app.get('/projet-create', (req, res) => {
        GProjetController.showForm(req, res)
    });

    app.post('/projet-create', (req, res) => {
        GProjetController.create(req, res)
    });



    app.get('/projet-update/:idprojets', (req, res) => {
        GProjetController.showFormUpdate(req, res)
    });

    app.post('/projet-update/:idprojets', (req, res) => {
        GProjetController.update(req, res)
    });

    app.get('/projet-delete/:idprojets', (req, res) => {
        ArticleController.delete(req, res)
    });
}
